CREATE USER PUGHADE_DB IDENTIFIED BY PUGHADE;

GRANT CREATE SESSION, CREATE TABLE, CREATE VIEW TO PUGHADE_DB;


CREATE TABLE CUSTOMER (CUSTOMER_ID NUMBER (20) CONSTRAINT CUSTOMER_ID_PK PRIMARY KEY,
			USERNAME VARCHAR2(100), USER_PASSWORD VARCHAR2(50),
			FIRST_NAME CHAR(100),
			LAST_NAME CHAR(30),
			EMAIL_ADDRESS VARCHAR2(50),
			PHONE_NUMBER VARCHAR2(20),
			ACCOUNT_CREATION timestamp,
			CONSTRAINT email_uk UNIQUE(email_address));

INSERT INTO Customer (customer_id, first_name, last_name, email_address, username, phone_number, user_password, account_creation) 
(SELECT customer_id, first_name, last_name, email_address, username, phone_number, user_password, account_creation FROM customer_1);


create sequence seq
start with 1
increment by 1;

CREATE TABLE SHIPPING (shipping_ID NUMBER(10) CONSTRAINT shipping_ID_PK PRIMARY KEY,
			ADDRESS VARCHAR2(150), CITY varCHAR2(50),
			STATE varCHAR2(50), ZIPCODE VARCHAR2(20),
			COUNTRY VARCHAR2(20),
			CUSTOMER_ID NUMBER(20),
      CONSTRAINT SHIPPING_CUSTOMER_FK FOREIGN KEY(CUSTOMER_ID) REFERENCES CUSTOMER(CUSTOMER_ID));

INSERT INTO shipping (shipping_id, address, city, state, zipcode, country, customer_id) 
(SELECT seq.nextval, address, city, state, zipcode, country, user_id FROM shipping_1);


CREATE TABLE PAYMENTS(PAYMENT_ID varchar2(50) CONSTRAINT PAYMENT_ID_PK PRIMARY KEY,
      address varchar2(100),
      city varchar2(50),
      state varchar2(50),
      country varchar2(50),
      zipcode varchar2(50),
      CUSTOMER_ID NUMBER(20),
      CONSTRAINT PAYMENTS_CUSTOMER_FK FOREIGN KEY(CUSTOMER_ID) REFERENCES CUSTOMER(CUSTOMER_ID));

INSERT INTO payments (payment_id, address, city, state, zipcode, country, customer_id) 
(SELECT distinct token, address, city, state, country, zipcode, payment_id FROM payments_1);

CREATE TABLE PRODUCT(product_id number(20) CONSTRAINT PRODUCT_ID_PK PRIMARY KEY,
      product_number varchar2(50),
      TITLE VARCHAR2(150),
      DESCRIPTION VARCHAR2(300),
      product_category VARCHAR2(100),
      MANUFACTURER VARCHAR2(50),
      product_FINISH VARCHAR2(200));

INSERT INTO product (product_id, product_number, TITLE, description, product_category, manufacturer, product_finish) 
(SELECT unique_id, product_id, title, description, productcategory, manufacturer, productfinish FROM product_1);
	
CREATE TABLE ORDERS(ORDER_ID NUMBER(20) CONSTRAINT ORDER_ID_PK PRIMARY KEY,
      CUSTOMER_ID NUMBER(20),
      shipping_id NUMBER(10),
      payment_id varchar2(50),
      ORDER_DATE timestamp,
      STATUS VARCHAR2(20),
      order_group_id varchar2(50) unique,
      site varchar2(50),
      CONSTRAINT ORDERS_CUSTOMER_FK FOREIGN KEY(CUSTOMER_ID) REFERENCES CUSTOMER(CUSTOMER_ID),
      CONSTRAINT ORDERS_shipping_FK FOREIGN KEY(shipping_ID) REFERENCES shipping(shipping_ID),
      constraint orders_payment_fk foreign key(payment_id) references payments(payment_id));

INSERT INTO orders (order_id, customer_id, order_date, status, order_group_id, site, shipping_id, payment_id) 
(SELECT order_number, user_id, date_ordered, status, order_group_id, site, NULL, NULL FROM order_1);

UPDATE orders ot
SET ot.shipping_id =
(SELECT MAX(st.shipping_id) FROM shipping st WHERE st.customer_id = ot.customer_id);

UPDATE orders ot
SET ot.payment_id =
(SELECT MAX(tp.payment_id) FROM payments tp WHERE tp.customer_id = ot.customer_id)

create sequence order_item_seq
start with 1
minvalue 0
increment by 1

create table order_items ( order_item_id number(10) constraint order_item_id_pk primary key,
      order_group_id varchar2(50) references orders(order_group_id),
      product_id number(20) references product(product_id),
      unit_price number(30,3),
      quantity number(10),
      total_tax number(30,3));

INSERT INTO order_items (order_item_id, order_group_id, product_id, unit_price, quantity, total_tax) 
(SELECT order_item_seq.nextval, cart_id, product_id, unit_price, quantity, total_tax FROM order_item_1);

CREATE TABLE PURCHASE_ORDER(PURCHASE_ORDER_ID NUMBER(10) CONSTRAINT PURCHASE_ORDER_ID_PK PRIMARY KEY,
      ORDER_ID NUMBER(20) references orders(order_id),
      status varchar2(50),
      vendor varchar2(50),
      PURCHASE_ORDER_DATE timestamp);  

INSERT INTO purchase_order (purchase_order_id, order_id, status, vendor, purchase_order_date) 
(SELECT purchase_order_id, order_id, status, vendor, podate FROM purchase_order_1);

CREATE TABLE purchase_order_item
(purchaseorder_item_id NUMBER(32) primary key, 
purchase_orderid NUMBER(20) REFERENCES purchase_order(purchase_order_id),
product_id NUMBER(20) REFERENCES product(product_id),
product_number VARCHAR2(50),
manufacturer VARCHAR2(50),
finish VARCHAR2(150),
quantity NUMBER(20),
unit_price NUMBER(10,5));

create sequence purchase_order_item_seq
start with 1
minvalue 0
increment by 1

INSERT INTO PURCHASE_ORDER_ITEM
(PURCHASEORDER_ITEM_ID, PURCHASE_ORDERID,
 PRODUCT_NUMBER,MANUFACTURER,FINISH,QUANTITY,UNIT_PRICE)
SELECT PURCHASE_ORDER_ITEM_seq.NEXTVAL,PURCHASE_ID,
PRODUCT_NUMBER,
MANUFACTURER, FINISHED,QUANTITY,UNIT_PRICE FROM PURCHASEORDER_ITEM_TEMP;


update purchase_order_item p
set p.product_id = 
(select max(pr.product_sku) from product pr
 where p.product_number = pr.product_id);
                       

