
Procedures 1:
create or replace procedure updatePassword
      (user_id IN varchar2,pass IN varchar2)
      IS
        
      BEGIN 
        UPDATE CUSTOMER
        SET user_PASSWORD = pass
        where customer_id = user_id;
      EXCEPTION
      WHEN OTHERS THEN 
        raise_application_error(-2000,'Error occurrence - '||SQLCODE||' error');
      END;
exec updatePassword(1229035,'bass')
select * from customer where customer_id = 1229035;

Procedures 2:
create or replace procedure UPDATE_STATUS
      (ORDER_NO IN varchar2,NEW_STATUS IN varchar2)
      IS        
      BEGIN 
        UPDATE ORDERs 
        SET STATUS = NEW_STATUS
        where ORDER_ID = ORDER_NO;
      EXCEPTION
      WHEN OTHERS THEN 
        raise_application_error(-2000,'Error occurrence - '||SQLCODE||' error');
      END;
exec update_status(56122043,'under review')
select * from orders where order_id = 56122043;




function1:

create or replace function daily_sales_total
(site_name in varchar2,search_date in varchar)
return number
is 
TOTAL_SALES NUMBER(10,3);
Begin  
select sum(quantity*unit_price) into total_sales from order_items
where order_group_id in ( 
  select order_group_id from orders 
  where lower(status) <> 'cancelled' and trunc(order_date) = to_date(search_date,'DD-MM-YY') and site = site_name);
  if total_sales <> 0 Then
  return total_sales;
  end if;
  return NULL;
end;


select sum(quantity*unit_price) from order_items
where order_group_id in ( 
  select order_group_id from orders 
  where trunc(order_date) = to_date('01-01-10','DD-MM-YY') and site = 'wildcat.com');
  
set serveroutput on
execute dbms_output.put_line(daily_sales_total('wildcat.com','01-01-10'));



function2:

create or replace function daily_profit
(site_name in varchar2,search_date in varchar)
return number
is 
  TOTAL_SALES NUMBER(10,3);
  TOTAL_COST  NUMBER(10,3);
  DIFFERENCE  NUMBER(10,3);
Begin  
  select sum(quantity*unit_price) into total_sales from order_items
  where order_group_id in ( 
    select order_group_id from orders 
    where lower(status) <> 'cancelled' 
    and trunc(order_date) = to_date(search_date,'DD-MM-YY') 
    and site = site_name
    );

  select sum(quantity * unit_price) into total_cost from purchase_order_item
  where purchase_orderid in 
    (
      select purchase_order_id from purchase_order
      where order_id in ( 
          select order_id from orders 
          where lower(status) <> 'cancelled' 
          and trunc(order_date) = to_date(search_date,'DD-MM-YY') 
          and site = site_name
        )
    );
    
    difference := total_sales - total_cost;
    
    if difference <> 0 Then
      return difference;
    end if;
  return NULL;
end;

set serveroutput on;
exec dbms_output.put_line(daily_profit('wildcat.com','01-01-10'));




select * from product;
select * from order_items;
select * from orders;
----------------------------report 1-----------------------------------
select title,total_items_sold,product_identifier
from
(
select prod_title as title,sum(amount) as total_items_sold,prod_id as product_identifier
from(
select pt.title as prod_title,
       oct.quantity as amount,
       ot.site, 
       pt.product_id as prod_id
from product pt
inner join order_items oct
  on oct.product_id = pt.product_id
inner join orders ot
  on ot.order_group_id = oct.order_group_id
where ot.site = 'wildcat.ca')
group by prod_title,prod_id
order by 2 desc)
where rownum<11;

--for wildcat.com
select title,total_items_sold,product_identifier
from
(
select prod_title as title,sum(amount) as total_items_sold,prod_id as product_identifier
from(
select pt.title as prod_title,
       oct.quantity as amount,
       ot.site, 
       pt.product_id as prod_id
from product pt
inner join order_items oct
  on oct.product_id = pt.product_id
inner join orders ot
  on ot.order_group_id = oct.order_group_id
where ot.site = 'wildcat.com')
group by prod_title,prod_id
order by 2 desc)
where rownum<11;
----------------------------------------------------------------------------
 select sum(quantity*unit_price) into total_sales from order_items
  where order_group_id in ( 
    select order_group_id from orders 
    where lower(status) <> 'cancelled' 
    and trunc(order_date) = to_date('01-01-10','DD-MM-YY') 
    and site = 'wildcat.com')
 
select * from orders;
select * from order_items;

   
 --------------------------------report2-----------------------------------
 select sum(oct.quantity*oct.unit_price),trunc(ot.order_date) 
 from orders ot
 inner join order_items oct
 on oct.order_group_id = ot.order_group_id
 where ot.site = 'wildcat.ca' and lower(ot.status) <> 'cancelled'
 group by trunc(ot.order_date)
 order by 2;
 -----------------------------------------------------------------------
--for wildcat.com

 select sum(oct.quantity*oct.unit_price),trunc(ot.order_date) 
 from orders ot
 inner join order_items oct
 on oct.order_group_id = ot.order_group_id
 where ot.site = 'wildcat.com' and lower(ot.status) <> 'cancelled'
 group by trunc(ot.order_date)
 order by 2;