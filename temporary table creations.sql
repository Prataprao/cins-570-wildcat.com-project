

CREATE TABLE CUSTOMER_1 (CUSTOMER_ID NUMBER (20),
			USERNAME VARCHAR2(100), USER_PASSWORD VARCHAR2(50),
			FIRST_NAME CHAR(100),
			LAST_NAME CHAR(30),
			EMAIL_ADDRESS VARCHAR2(50),
			PHONE_NUMBER VARCHAR2(20),
			ACCOUNT_CREATION timestamp);

drop table payments_1;
CREATE TABLE PAYMENTS_1 (PAYMENT_ID NUMBER(20),
			TOKEN VARCHAR2(100),
      address varchar2(100),
      city varchar2(50),
      state varchar2(50), zipcode varchar2(50),
      country varchar2(50),
      card_type CHAR(10));

update shipping_1
set country='CANADA'
where country is NULL;

drop table order_1;      
CREATE TABLE ORDER_1 (order_number number(20),
      user_id number(20),
      status varchar2(50),
      date_ordered timestamp,
      order_group_id varchar2(50),
      site varchar2(50));

drop table order_item_1;      
create table order_item_1 (cart_id varchar2(100),
      product_id varchar2(50),
      unit_price number(30,3),
      quantity number(10),
      total_tax number(30,3));

select * 
from order_item_1;
drop table shipping_1;
create table shipping_1 ( user_id number(20),
      address varchar2(100),
      city varchar2(50),
      state varchar2(50),
      zipcode varchar2(100),
      country varchar2(20));

update shipping_1
set country='CANADA'
where country is NULL;
      

create table purchase_order_1( purchase_order_id number(20),
      order_id number(20),
      vendor varchar2(50),
      status varchar2(50),
      podate timestamp);

CREATE TABLE PurchaseOrder_Item_temp
(purchase_order_item_id NUMBER(32),
purchase_id NUMBER(32),
product_id NUMBER(32),
product_number VARCHAR2(32),
manufacturer VARCHAR2(100),
finished VARCHAR2(150),
quantity NUMBER(10),
unit_price NUMBER(10,5));
select * from purchaseorder_item_temp
    
create table product_1( product_id varchar2(50),
      unique_id number(20),
      manufacturer varchar2(50),
      productfinish varchar2(50),
      title varchar2(50),
      description varchar2(300),
      productcategory varchar2(100))
